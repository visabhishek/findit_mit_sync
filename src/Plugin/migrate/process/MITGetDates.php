<?php

namespace Drupal\findit_mit_sync\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "findit_mit_get_dates"
 * )
 */
class MITGetDates extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a findit_mit_get_dates process plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ClientInterface $http_client) {
    $configuration += [
      'base_url' => 'http://calendar.mit.edu/api/2/events/',
      'null_end_date_offset' => '+2 hours',
      'guzzle_options' => [
        'headers' => [
          'Accept' => 'application/json; charset=utf-8',
          'Accept-Encoding' => 'gzip, deflate',
        ],
      ],
    ];
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $raw_start = $row->get($this->configuration['start']);
    $raw_end = $row->get($this->configuration['end']);
    $recurring = $row->get($this->configuration['recurring']);
    $processed_dates = [];

    $fromFormat = 'Y-m-d\TH:i:sP';
    $toFormat = 'Y-m-d\TH:i:s';
    $from_timezone = 'America/New_York';
    $to_timezone = 'UTC';

    $transformed_start = DateTimePlus::createFromFormat($fromFormat, $raw_start, $from_timezone);
    $transformed_end = $this->getTransformedEndDate($fromFormat, $raw_end, $from_timezone, $transformed_start);

    $processed_dates[] = [
      'start_date' => $transformed_start->format($toFormat, ['timezone' => $to_timezone]),
      'end_date' => $transformed_end->format($toFormat, ['timezone' => $to_timezone]),
    ];

    if ($recurring) {
      $events_instances = $this->fetchIndividualEventInstances($row->getSourceProperty('src_unique_id'));
      if (is_array($events_instances)) {
        foreach ($events_instances as $events_instance) {
          $instance_start = DateTimePlus::createFromFormat($fromFormat, $events_instance['start'], $from_timezone);

          if ($instance_start > $transformed_start) {
            $instance_end = $this->getTransformedEndDate($fromFormat, $events_instance['end'], $from_timezone, $instance_start);
            $processed_dates[] = [
              'start_date' => $instance_start->format($toFormat, ['timezone' => $to_timezone]),
              'end_date' => $instance_end->format($toFormat, ['timezone' => $to_timezone]),
            ];
          }
        }
      }
    }

    return $processed_dates;
  }

  /**
   * @param string $fromFormat
   *   Date format to convert from.
   * @param string|NULL $raw_end
   *   Raw time value to convert from.
   * @param string $from_timezone
   *   Timezone to convert from.
   * @param \Drupal\Component\Datetime\DateTimePlus $transformed_start
   *   If $raw_end is NULL, use this value to calculate an end date out of a
   *   fixed offset.
   *
   * @return \Drupal\Component\Datetime\DateTimePlus
   */
  protected function getTransformedEndDate($fromFormat, $raw_end, $from_timezone, $transformed_start) {
    if (!empty($raw_end)) {
      return DateTimePlus::createFromFormat($fromFormat, $raw_end, $from_timezone);
    }
    else {
      $transformed_end = clone $transformed_start;
      return $transformed_end->add(\DateInterval::createFromDateString($this->configuration['null_end_date_offset']));
    }
  }

  /**
   * @param int $eventId
   *   ID of the event to fetch.
   *
   * @return mixed
   *    An array of event date instances or NULL if they request fails.
   *
   * @throws \Drupal\migrate\MigrateException
   *
   * @see \Drupal\migrate_plus\Plugin\migrate_plus\data_fetcher\Http::getResponseContent
   * @see \Drupal\migrate_plus\Plugin\migrate_plus\data_parser\Json::getSourceData
   *
   */
  protected function fetchIndividualEventInstances($eventId) {
    $url = $this->configuration['base_url'] . $eventId;
    $source_data = NULL;

    try {
      $response = $this->httpClient->get($url, $this->configuration['guzzle_options']);
      if (empty($response)) {
        throw new MigrateException('No response at ' . $url . '.');
      }

      $response_body = $response->getBody();

      // Convert objects to associative arrays.
      $source_data = json_decode($response_body, TRUE);

      // If json_decode() has returned NULL, it might be that the data isn't
      // valid utf8 - see http://php.net/manual/en/function.json-decode.php#86997.
      if (is_null($source_data)) {
        $utf8response = utf8_encode($response_body);
        $source_data = json_decode($utf8response, TRUE);
      }

      $selectors = explode('/', trim('/event/event_instances', '/'));
      foreach ($selectors as $selector) {
        if (!empty($selector)) {
          $source_data = $source_data[$selector];
        }
      }

      $source_data = array_column($source_data, 'event_instance');
    }
    catch (RequestException $e) {
      throw new MigrateException('Error message: ' . $e->getMessage() . ' at ' . $url . '.');
    }

    return $source_data;
  }

}
